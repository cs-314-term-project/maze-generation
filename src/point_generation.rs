//! Maze Generation Point Utilities
//!
//! This module provides utility functions for generating points as input to maze generation algorithms.
//! It includes methods to create structured point grids, circular point distributions with adjustable
//! variance, and point clouds derived from these distributions with specified density.
//! The generated points are intended for spatial distribution in maze generation, ensuring
//! an even and controllable spread of points across the maze area.

use itertools::Itertools;
use rand::{seq::SliceRandom, thread_rng, Rng};
use std::f32::consts::PI;

///! Generates a grid of 2D points with specified dimensions.
/// This function creates a Cartesian product of points ranging from (0, 0) to (x - 1, y - 1).
/// Primarily used as input for maze generation, providing a structured layout of points.
/// # Arguments
/// * `x` - The width of the grid.
/// * `y` - The height of the grid.
/// # Returns
/// Option<Vec<(u16, u16)>> - A vector of 2D tuples representing grid points, or None if the vector size is too large.
pub fn make_point_grid(x: u16, y: u16) -> Option<Vec<(u16, u16)>> {
    //Assert that the total length of the vector will be indexable by a usize
    usize::from(x).checked_mul(usize::from(y))?;
    //Return the cartesian product of (0..x) and (0..y)
    Some(
        (0..y)
            .flat_map(|y| (0..x).map(move |x| (x, y)))
            .collect_vec(),
    )
}

///! Generates a circle of approximately equidistant points.
/// This function creates a spiral of points with applied variance for spatial distribution in maze generation.
/// The points are normalized to fit within a range of 0 to 1 for consistency.
/// # Arguments
/// * `n_points` - The number of points to generate in the spiral.
/// * `variance` - The randomness applied to each point's position.
/// # Returns
/// Vec<(f32, f32)> - A vector of 2D tuples representing points in a circular pattern.

fn make_point_circle(n_points: usize, variance: f32) -> Vec<(f32, f32)> {
    assert!(n_points > 2, "Point circle requires at least 3 points.");
    let points = generate_spiral_points(n_points, variance);
    normalize_points(points)
}

///! Generates points in a spiral pattern with applied variance.
/// This function is a helper for `make_point_circle`, creating points in a spiral,
/// with each point's position offset by a random amount within the variance range.
/// # Arguments
/// * `n_points` - The number of points to generate.
/// * `variance` - The variance applied to each point's position.
/// # Returns
/// Vec<(f32, f32)> - A vector of 2D tuples representing the spiral points.
fn generate_spiral_points(n_points: usize, variance: f32) -> Vec<(f32, f32)> {
    let mut points = Vec::with_capacity(n_points);
    let mut angle: f32 = 2.0 * PI;
    let mut radius: f32 = 0.0;
    let distance_between_points = 1.0;
    let angle_increment_base = 0.1;

    for _ in 0..n_points {
        let x = radius * angle.cos() + thread_rng().gen::<f32>() * variance * 2.0 - variance;
        let y = radius * angle.sin() + thread_rng().gen::<f32>() * variance * 2.0 - variance;
        points.push((x, y));

        let circumference = 2.0 * PI * radius;
        let angle_increment = if radius < 1.0 {
            angle_increment_base
        } else {
            (distance_between_points / circumference) * (2.0 * PI)
        };

        angle += angle_increment;
        radius = angle / (2.0 * PI);
    }

    points
}

///! Normalizes a collection of points to fit within a 0 to 1 range.
/// This function scales and translates a set of points so that all coordinates
/// lie within a 0 to 1 range, preserving their relative positions.
/// # Arguments
/// * `points` - The vector of points to normalize.
/// # Returns
/// Vec<(f32, f32)> - The normalized vector of points.
fn normalize_points(mut points: Vec<(f32, f32)>) -> Vec<(f32, f32)> {
    let (min_x, max_x) = points
        .iter()
        .map(|&(x, _)| x)
        .minmax()
        .into_option()
        .expect("Failed to find min and max X values");
    let (min_y, max_y) = points
        .iter()
        .map(|&(_, y)| y)
        .minmax()
        .into_option()
        .expect("Failed to find min and max Y values");

    points.iter_mut().for_each(|(x, y)| {
        *x = (*x - min_x) / (max_x - min_x);
        *y = (*y - min_y) / (max_y - min_y);
    });

    points
}

///! Generates a point cloud from a spiral distribution based on specified density.
/// This function selects a subset of points from a generated spiral, based on the specified density,
/// to create a point cloud for maze generation. Higher density results in more points being selected.
/// # Arguments
/// * `n_points` - The number of points to include in the cloud.
/// * `density` - The density of the points in the spiral used to form the cloud.
/// * `variance` - The variance applied to the position of each point in the spiral.
/// # Returns
/// Vec<(f32, f32)> - A vector of 2D tuples representing the point cloud.
pub fn make_point_cloud(n_points: usize, density: f32, variance: f32) -> Vec<(f32, f32)> {
    assert!(
        (density > 0.0) && (density <= 1.0),
        "Scarcity must be between 0 and 1"
    );
    //Get the closest correct number of points, given the density.
    let adjusted_n_points = ((n_points as f32) / density).round() as usize;
    //Choose n_points random points from the point circle.
    make_point_circle(adjusted_n_points, variance)
        .choose_multiple(&mut thread_rng(), n_points)
        .copied()
        .collect()
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};

    use super::*;
    use delaunator::{triangulate, Point};

    fn distance(point_a: (f32, f32), point_b: (f32, f32)) -> f32 {
        let (dx, dy) = (point_a.0 - point_b.0, point_a.1 - point_b.1);
        (dx.powi(2) + dy.powi(2)).sqrt()
    }

    ///! Calculate a list of distances between each point & their 4 closest neighbors.
    /// This got... convl
    fn calculate_closest_neighbor_distances(points: &[(f32, f32)]) -> Vec<f32> {
        let delaunay_points: Vec<Point> = points
            .iter()
            .map(|&(x, y)| Point {
                x: x as f64,
                y: y as f64,
            })
            .collect();

        let triangulation = triangulate(&delaunay_points);
        let mut neighbors_by_point: HashMap<usize, HashSet<usize>> = HashMap::new();

        // Collect neighbors for each point
        for triangle in triangulation.triangles.chunks_exact(3) {
            for &i in triangle {
                let entry = neighbors_by_point.entry(i).or_insert_with(HashSet::new);
                for &j in triangle {
                    if i != j {
                        entry.insert(j);
                    }
                }
            }
        }

        // Calculate closest distances
        let mut distances: Vec<f32> = vec![];
        for (&i, neighbors) in &neighbors_by_point {
            let mut i_distances: Vec<f32> = neighbors
                .iter()
                .map(|&j| distance(points[i], points[j]))
                .collect();

            i_distances.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
            distances.extend(i_distances.into_iter().take(4));
        }

        distances
    }
    ///! Calculate the coefficient of variation of the distance between each point and its 3
    /// closest neighbors. A low coefficient indicates an even distribution of points.
    fn calculate_distance_coefficient_of_variation(points: &[(f32, f32)]) -> f32 {
        let distances = calculate_closest_neighbor_distances(points);
        let mean_distance = distances.iter().sum::<f32>() / distances.len() as f32;
        let variance: f32 = distances
            .iter()
            .map(|&distance| (distance - mean_distance).powi(2))
            .sum::<f32>()
            / distances.len() as f32;
        let std_dev = variance.sqrt();
        std_dev / mean_distance
    }

    #[test]
    fn test_make_point_grid() {
        assert_eq!(
            make_point_grid(2, 3),
            Some(vec![(0, 0), (1, 0), (0, 1), (1, 1), (0, 2), (1, 2)])
        );
    }

    #[test]
    fn test_make_point_cloud() {
        let point_cloud = make_point_cloud(256, 0.5, 1.0);
        assert_eq!(point_cloud.len(), 256);
        assert!(point_cloud
            .iter()
            .all(|(x, y)| { (*x <= 1.0) && *y <= 1.0 }));
        assert!(point_cloud
            .iter()
            .all(|(x, y)| { (*x >= 0.0) && *y >= 0.0 }));
    }

    #[test]
    fn test_make_point_grid_large_values() {
        let grid = make_point_grid(1000, 1000).unwrap();
        assert_eq!(grid.len(), 1_000_000);
    }

    ///! Test that increasing variance increases the standard deviation of inter-point distance.
    #[test]
    fn test_make_point_circle_variance_impact() {
        let points_low_variance = make_point_circle(100, 0.0);
        let points_high_variance = make_point_circle(100, 1.0);

        let std_dev_low = calculate_distance_coefficient_of_variation(&points_low_variance);
        let std_dev_high = calculate_distance_coefficient_of_variation(&points_high_variance);

        assert!(
            std_dev_low < std_dev_high,
            "Low variance should result in more consistent spacing than high variance"
        );
    }

    ///! Test that the distribution of points with no variance is within tolerance.
    #[test]
    fn test_make_point_circle_point_distribution() {
        let points = make_point_circle(100, 0.0);
        let threshold = 0.2; // 20% coefficient of variation as the threshold
        let cv = calculate_distance_coefficient_of_variation(&points);
        assert!(
            cv < threshold,
            "Points are not approximately equidistant. {cv} > {threshold}."
        );
    }

    #[test]
    fn test_make_point_cloud_count_accuracy() {
        let n_points = 100;
        let point_cloud = make_point_cloud(n_points, 0.5, 1.0);
        assert_eq!(point_cloud.len(), n_points);
    }
}

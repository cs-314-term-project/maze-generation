//! Maze Generation and Analysis Module
//!
//! This module provides the functionality to generate, manipulate, and analyze mazes.
//! It includes structures and algorithms for creating a maze from a set of points,
//! determining connections between nodes, and modifying these connections. Key features
//! include Delaunay triangulation for initial setup, functions for breaking and making
//! connections between nodes, and scoring algorithms for different aspects of the maze such as
//! twistiness, branchiness, and the formation of hallways. The module allows for creating
//! mazes with varying complexity and characteristics based on specified weights and criteria.
//!
//! Key Structures:
//! - `Maze<T>`: Represents the maze with nodes placed at given coordinates.
//! - `MazeNode<T>`: Represents a single node within the maze, tracking its connections.
//! - `MazeGenerationWeights`: Holds weights for scoring potential connections in the maze.
//!
//! Key Functions:
//! - `Maze::new`: Initializes a new maze with given points.
//! - `Maze::count_connections`: Counts all two-way connections in the maze.
//! - `Maze::triangulate_nodes`: Establishes initial connections between nodes based on Delaunay triangulation.
//! - `Maze::break_connection`: Breaks an existing connection between two nodes.
//! - `Maze::make_connection`: Creates a new connection between two nodes.
//! - Scoring functions (`calc_hallway_score`, `calc_twisty_score`, `calc_branchy_score`):
//!   Evaluate different aspects of the maze's layout.
//!
//! This module is intended for use in applications that require maze generation and analysis,
//! such as games, simulations, and educational tools.

use std::collections::HashSet;

use delaunator::{next_halfedge, triangulate, Point};
use itertools::Itertools;
use ordered_float::OrderedFloat;
use rand::{thread_rng, Rng};
///! Represents a single node within a Maze object.
/// Contains position data, sets of indices for connected and broken neighbors.
#[derive(Debug)]
pub struct MazeNode<T> {
    pub pos: (T, T),
    pub connected_neighbors: HashSet<usize>,
    pub broken_neighbors: HashSet<usize>,
}

///! A trait defining types that can be used as coordinates in a Maze.
/// Requires Copy, PartialOrd, conversion into f64, and Debug traits.
pub trait MazeCoordType: Copy + PartialOrd + Into<f64> + std::fmt::Debug {}
impl MazeCoordType for f32 where f64: From<f32> {}
impl MazeCoordType for u16 where f64: From<u16> {}

///! Holds weights for different aspects of maze generation: twists, branches, hallways, and randomness.
/// Used to influence the maze generation algorithm's behavior.
#[derive(Default)]
pub struct MazeGenerationWeights {
    pub twists: f64,
    pub branches: f64,
    pub hallways: f64,
    pub random: f64,
}

///! Represents a maze, consisting of a set of connected nodes within specified bounds.
pub struct Maze<T: MazeCoordType> {
    pub bounds: (T, T),
    pub nodes: Vec<MazeNode<T>>,
}

impl<T: MazeCoordType> Maze<T>
where
    f64: From<T>,
{
    ///! Creates a new Maze instance.
    /// Given a set of points representing node coordinates, this function initializes the maze.
    /// If `connect_neighbors` is true, initial connections between neighboring nodes are established;
    /// otherwise, they are marked as potential but not active connections.
    /// # Arguments
    /// * `points` - Coordinates of nodes in the maze.
    /// * `connect_neighbors` - Determines whether to initially connect the neighbors or leave them broken.
    pub fn new(points: &[(T, T)], connect_neighbors: bool) -> Maze<T>
    where
        f64: From<T>,
    {
        if points.len() < 3 {
            panic!("Must provide at least three points.");
        }
        assert!(
            points
                .iter()
                .all(|(x, y)| (f64::from(*x) >= 0.0) & (f64::from(*y) >= 0.0)),
            "All maze coordinates must be positive."
        );

        let max_x = points
            .iter()
            .map(|(x, _)| *x)
            .max_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal))
            .unwrap();

        let max_y = points
            .iter()
            .map(|(_, y)| *y)
            .max_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal))
            .unwrap();

        let bounds = (max_x, max_y);

        let nodes = Self::triangulate_nodes(points, connect_neighbors);
        Self { nodes, bounds }
    }

    ///! Counts the total number of two-way connections in the maze.
    /// This function iterates through all nodes, counting each unique two-way path between nodes.
    pub fn count_connections(&self) -> usize {
        self.nodes
            .iter()
            .enumerate()
            .flat_map(|(i, n)| n.connected_neighbors.iter().filter(move |&&j| i < j))
            .count()
    }

    ///! Triangulates nodes to determine initial connections or potential connections.
    /// This function uses Delaunay triangulation to establish initial connections between nodes
    /// based on the `connect_neighbors` flag.
    /// # Arguments
    /// * `points` - Coordinates of nodes to be triangulated.
    /// * `connect_neighbors` - Flag to determine whether to connect neighbors or leave connections as potential.
    fn triangulate_nodes(points: &[(T, T)], connect_neighbors: bool) -> Vec<MazeNode<T>> {
        let mut nodes: Vec<MazeNode<T>> = points
            .into_iter()
            .map(|pos| MazeNode {
                pos: *pos,
                broken_neighbors: HashSet::default(),
                connected_neighbors: HashSet::default(),
            })
            .collect_vec();
        //Convert points to Point objects expected by Triangulate
        let delaunay_points: Vec<Point> = points
            .iter()
            .map(|(x, y)| Point {
                x: (*x).into(),
                y: (*y).into(),
            })
            .collect();

        let result = triangulate(&delaunay_points);
        for i in 0..(result.triangles.len() / 3) {
            for a in (i * 3)..(i * 3 + 3) {
                let b = next_halfedge(a);
                let vertex_a = result.triangles[a];
                let vertex_b = result.triangles[b];
                if connect_neighbors & !nodes[vertex_a].connected_neighbors.contains(&vertex_b) {
                    nodes[vertex_a].connected_neighbors.insert(vertex_b);
                    nodes[vertex_b].connected_neighbors.insert(vertex_a);
                } else if !connect_neighbors & !nodes[vertex_a].broken_neighbors.contains(&vertex_b)
                {
                    nodes[vertex_a].broken_neighbors.insert(vertex_b);
                    nodes[vertex_b].broken_neighbors.insert(vertex_a);
                }
            }
        }
        nodes
    }

    ///! Breaks an existing connection between two nodes in the maze.
    /// This function removes an active connection and marks it as a broken neighbor.
    /// It asserts that the nodes are different, the indices are in range, and an existing connection is present.
    /// # Arguments
    /// * `a` - Index of the first node.
    /// * `b` - Index of the second node.
    pub fn break_connection(&mut self, a: usize, b: usize) {
        debug_assert!(
            a != b,
            "Cannot break connection between a node and itsself."
        );
        debug_assert!(
            (a < self.nodes.len()) || (b < self.nodes.len()),
            "Index out of range."
        );
        debug_assert!(
            self.nodes[a].connected_neighbors.contains(&b),
            "Node #{b} not connected to #{a}."
        );
        debug_assert!(
            self.nodes[b].connected_neighbors.contains(&a),
            "Node #{a} not connected to #{b}."
        );
        self.nodes[a].connected_neighbors.remove(&b);
        self.nodes[b].connected_neighbors.remove(&a);
        self.nodes[a].broken_neighbors.insert(b);
        self.nodes[b].broken_neighbors.insert(a);
    }

    ///! Establishes a connection between two nodes in the maze.
    /// This function adds a new active connection between nodes and removes them from broken neighbors.
    /// It asserts that the nodes are different, the indices are in range, and the connection is currently broken.
    /// # Arguments
    /// * `a` - Index of the first node.
    /// * `b` - Index of the second node.
    pub fn make_connection(&mut self, a: usize, b: usize) {
        debug_assert!(a != b, "Cannot make connection between a node and itsself.");
        debug_assert!(
            (a < self.nodes.len()) || (b < self.nodes.len()),
            "Index out of range."
        );
        debug_assert!(
            self.nodes[a].broken_neighbors.contains(&b),
            "Node #{b} can't connect to #{a}."
        );
        debug_assert!(
            self.nodes[b].broken_neighbors.contains(&a),
            "Node #{a} can't connect to #{b}."
        );
        self.nodes[a].broken_neighbors.remove(&b);
        self.nodes[b].broken_neighbors.remove(&a);
        self.nodes[a].connected_neighbors.insert(b);
        self.nodes[b].connected_neighbors.insert(a);
    }

    ///! Calculates the cosine of the angle formed by three points (nodes) in the maze.
    /// This geometric measure is used to assess the twistiness of paths in the maze.
    /// # Arguments
    /// * `corner_left` - The left node of the corner.
    /// * `corner_center` - The central node of the corner.
    /// * `corner_right` - The right node of the corner.
    fn calculate_cosine_of_corner(
        corner_left: &MazeNode<T>,
        corner_center: &MazeNode<T>,
        corner_right: &MazeNode<T>,
    ) -> f64
    where
        f64: From<T>,
    {
        let v_c2l = (
            f64::from(corner_left.pos.0) - f64::from(corner_center.pos.0),
            f64::from(corner_left.pos.1) - f64::from(corner_center.pos.1),
        );
        let v_c2r = (
            f64::from(corner_right.pos.0) - f64::from(corner_center.pos.0),
            f64::from(corner_right.pos.1) - f64::from(corner_center.pos.1),
        );
        let v_c2l_mag = (v_c2l.0.powi(2) + v_c2l.1.powi(2)).sqrt();
        let v_c2r_mag = (v_c2r.0.powi(2) + v_c2r.1.powi(2)).sqrt();
        let dot = v_c2l.0 * v_c2r.0 + v_c2l.1 * v_c2r.1;
        dot / (v_c2l_mag * v_c2r_mag)
    }
}

///! Calculates a score indicating if a connection forms a hallway.
/// A connection is considered a hallway if adding it will not create any branches.
/// This means both connected nodes will have 2 or fewer connected neighbors.
/// The score gets a bonus if either hall is currently a stub (a short dead end)
/// # Arguments
/// * `cur_leaf_idx` - Index of the current node.
/// * `next_leaf_idx` - Index of the next node.
/// * `maze` - The maze structure.
fn calc_hallway_score<T: MazeCoordType>(
    cur_leaf_idx: usize,
    next_leaf_idx: usize,
    maze: &Maze<T>,
) -> f64 {
    let mut cur_stub_boost = 0.0;
    let mut next_stub_boost = 0.0;
    let cur_connected_count = maze.nodes[cur_leaf_idx].connected_neighbors.len();
    let next_connected_count = maze.nodes[next_leaf_idx].connected_neighbors.len();
    let cur_is_hallway = cur_connected_count <= 1;
    let next_is_hallway = maze.nodes[next_leaf_idx].connected_neighbors.len() <= 1;

    if cur_connected_count == 1 {
        let cur_neighbor_idx = *maze.nodes[cur_leaf_idx]
            .connected_neighbors
            .iter()
            .next()
            .unwrap();
        let cur_neighbor = &maze.nodes[cur_neighbor_idx];
        if cur_neighbor.connected_neighbors.len() > 2 {
            cur_stub_boost = 0.2;
        }
    }
    if next_connected_count == 1 {
        let next_neighbor_idx = *maze.nodes[next_leaf_idx]
            .connected_neighbors
            .iter()
            .next()
            .unwrap();
        let next_neighbor = &maze.nodes[next_neighbor_idx];
        if next_neighbor.connected_neighbors.len() > 2 {
            next_stub_boost = 0.2;
        }
    }

    f64::from(cur_is_hallway) * 0.3
        + f64::from(next_is_hallway) * 0.3
        + cur_stub_boost
        + next_stub_boost
}

///! Scores the twistiness of a connection based on the average turn angle to other paths.
/// A connection is twisty if the average turn angle between it and its nodes' other hallways is high.
/// # Arguments
/// * `cur_leaf_idx` - Index of the current node.
/// * `next_leaf_idx` - Index of the next node.
/// * `maze` - The maze structure.
/// I made a visualization here: https://www.desmos.com/calculator/z9ka7t5yqa
fn calc_twisty_score<T: MazeCoordType>(
    cur_leaf_idx: usize,
    next_leaf_idx: usize,
    maze: &Maze<T>,
) -> f64
where
    f64: From<T>,
{
    let cur_leaf = &maze.nodes[cur_leaf_idx];
    let next_leaf = &maze.nodes[next_leaf_idx];
    if cur_leaf.connected_neighbors.len() < 2 {
        return 0.0;
    }
    let mut sample_count = 0.0;
    // Sum the twistiness of all paths connected to this one
    let twist_sum: f64 = cur_leaf
        .connected_neighbors
        .iter()
        .chain(next_leaf.connected_neighbors.iter()) // Consider twistiness at both ends
        .map(|&idx| {
            let t = Maze::calculate_cosine_of_corner(next_leaf, &cur_leaf, &maze.nodes[idx]);
            (t + 1.0) / 2.0
        })
        .inspect(|_| sample_count += 1.0) //Count the samples as we go
        .sum();
    twist_sum / sample_count
}

///! Scores the branchiness of a connection.
/// A connection is considered branchy if the connected nodes will branch, i.e., have more than 2 connected neighbors.
/// # Arguments
/// * `cur_leaf_idx` - Index of the current node.
/// * `next_leaf_idx` - Index of the next node.
/// * `maze` - The maze structure.
fn calc_branchy_score<T: MazeCoordType>(
    cur_leaf_idx: usize,
    next_leaf_idx: usize,
    maze: &Maze<T>,
) -> f64 {
    let cur_will_branch = maze.nodes[cur_leaf_idx].connected_neighbors.len() > 1;
    let next_will_branch = maze.nodes[next_leaf_idx].connected_neighbors.len() > 1;
    f64::from(cur_will_branch) * 0.5 + f64::from(next_will_branch) * 0.5
}

///! Grades a potential connection using various criteria including twistiness, branchiness, and hallway likelihood.
/// Each aspect is scored and combined with respective weights, including a random factor, to compute the overall score.
/// # Arguments
/// * `maze` - The maze structure.
/// * `cur_leaf_idx` - Index of the current node.
/// * `next_leaf_idx` - Index of the next node.
/// * `weights` - Weights applied to different scoring metrics.
pub fn grade_potential_connection<T: MazeCoordType>(
    maze: &Maze<T>,
    cur_leaf_idx: usize,
    next_leaf_idx: usize,
    weights: &MazeGenerationWeights,
) -> OrderedFloat<f64>
where
    f64: From<T>,
{
    let t = calc_twisty_score(cur_leaf_idx, next_leaf_idx, maze) * weights.twists;
    let b = calc_branchy_score(cur_leaf_idx, next_leaf_idx, maze) * weights.branches;
    let h = calc_hallway_score(cur_leaf_idx, next_leaf_idx, maze) * weights.hallways;
    let r = thread_rng().gen::<f64>() * weights.random;
    let point_total = t + b + h + r;

    return OrderedFloat(point_total);
}

#[cfg(test)]
mod tests {
    use super::*;
    ///! Recursively traverse a maze, and log which nodes are visited.
    fn traverse<T: MazeCoordType>(maze: &Maze<T>, node_idx: usize, visited: &mut [bool]) {
        let node: &MazeNode<T> = &maze.nodes[node_idx];
        visited[node_idx] = true;
        let next_nodes: Vec<usize> = node
            .connected_neighbors
            .iter()
            .filter_map(|next_node| {
                if !visited[*next_node] {
                    Some(*next_node)
                } else {
                    None
                }
            })
            .collect();
        for next_node in next_nodes {
            traverse(maze, next_node, visited);
        }
    }

    ///! Check that a maze is connected (e.g. every point is reachable by every other point)
    fn is_connected<T: MazeCoordType>(maze: &Maze<T>) -> bool {
        let mut visited = vec![false; maze.nodes.len()];
        traverse(maze, 0, &mut visited);
        visited.iter().all(|x| *x)
    }

    #[test]
    fn test_connect_points_connected_u16() {
        let connected = Maze::<u16>::new(&[(0, 0), (1, 0), (0, 1)], true);
        assert!(is_connected(&connected));
        assert_eq!(connected.nodes.len(), 3);
        assert_eq!(
            connected.nodes[0].connected_neighbors,
            HashSet::from([2, 1])
        );
        assert_eq!(
            connected.nodes[1].connected_neighbors,
            HashSet::from([2, 0])
        );
        assert_eq!(
            connected.nodes[2].connected_neighbors,
            HashSet::from([0, 1])
        );
        for node in connected.nodes {
            assert_eq!(node.broken_neighbors, HashSet::default());
        }

        let connected = Maze::<u16>::new(&[(0, 0), (3, 0), (0, 3), (3, 3), (2, 2)], true);
        assert!(is_connected(&connected));
        assert_eq!(connected.nodes.len(), 5);
        assert_eq!(
            connected.nodes[0].connected_neighbors,
            HashSet::from([1, 4, 2])
        );
        assert_eq!(
            connected.nodes[1].connected_neighbors,
            HashSet::from([3, 4, 0])
        );
        assert_eq!(
            connected.nodes[2].connected_neighbors,
            HashSet::from([4, 3, 0])
        );
        assert_eq!(
            connected.nodes[3].connected_neighbors,
            HashSet::from([4, 1, 2])
        );
        assert_eq!(
            connected.nodes[4].connected_neighbors,
            HashSet::from([3, 1, 0, 2])
        );
    }

    #[test]
    fn test_connect_points_connected_f32() {
        let connected = Maze::<f32>::new(&[(0.0, 0.0), (1.0, 0.0), (0.0, 1.0)], true);
        assert!(is_connected(&connected));
        assert_eq!(connected.nodes.len(), 3);
        assert_eq!(
            connected.nodes[0].connected_neighbors,
            HashSet::from([2, 1])
        );
        assert_eq!(
            connected.nodes[1].connected_neighbors,
            HashSet::from([2, 0])
        );
        assert_eq!(
            connected.nodes[2].connected_neighbors,
            HashSet::from([0, 1])
        );
        for node in connected.nodes {
            assert_eq!(node.broken_neighbors, HashSet::default());
        }

        let connected = Maze::<f32>::new(
            &[(0.0, 0.0), (1.0, 0.0), (0.0, 1.0), (1.0, 1.0), (0.5, 0.5)],
            true,
        );
        assert!(is_connected(&connected));
        assert_eq!(connected.nodes.len(), 5);
        assert_eq!(
            connected.nodes[0].connected_neighbors,
            HashSet::from([4, 1, 2])
        );
        assert_eq!(
            connected.nodes[1].connected_neighbors,
            HashSet::from([4, 0, 3])
        );
        assert_eq!(
            connected.nodes[2].connected_neighbors,
            HashSet::from([0, 4, 3])
        );
        assert_eq!(
            connected.nodes[3].connected_neighbors,
            HashSet::from([2, 4, 1])
        );
        assert_eq!(
            connected.nodes[4].connected_neighbors,
            HashSet::from([0, 1, 2, 3])
        );

        for node in connected.nodes {
            assert_eq!(node.broken_neighbors, HashSet::default());
        }
    }

    #[test]
    fn test_connect_points_broken() {
        let broken = Maze::<u16>::new(&[(0, 0), (1, 0), (0, 1)], false);
        assert_eq!(broken.nodes.len(), 3);
        assert_eq!(broken.nodes[0].broken_neighbors, HashSet::from([2, 1]));
        assert_eq!(broken.nodes[1].broken_neighbors, HashSet::from([2, 0]));
        assert_eq!(broken.nodes[2].broken_neighbors, HashSet::from([0, 1]));
        for node in broken.nodes {
            assert_eq!(node.connected_neighbors, HashSet::default());
        }
    }

    #[test]
    #[should_panic]
    fn test_connect_points_panic() {
        Maze::<u16>::new(&[(0, 0), (1, 0)], true);
    }
}

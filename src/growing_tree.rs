//! Growing Tree Maze Generation
//!
//! This module implements the growing tree algorithm for maze generation, which is highly adaptable
//! and can generate a variety of maze styles. It includes functions for selecting the next expansion point (leaf),
//! clearing dead ends, and generating the maze structure. The algorithm's behavior is influenced by various
//! weights that determine aspects like twistiness, branchiness, hallway likelihood, and randomness in the maze.

use crate::{
    maze_generation::{grade_potential_connection, Maze, MazeCoordType, MazeGenerationWeights},
    MazeNode,
};
#[cfg(feature = "verbose")]
use indicatif::ProgressBar;
use rand::Rng;

///! Removes leaves that no longer have unvisited neighbors.
/// This function iterates through the current leaves in the maze and retains only those
/// that have potential for further expansion, i.e., have broken neighbors that have not been visited.
/// # Arguments
/// * `leaves` - A mutable reference to the vector of current leaf indices in the maze.
/// * `visited` - A slice indicating whether each node in the maze has been visited.
/// * `nodes` - A slice of `MazeNode`s representing the nodes in the maze.
fn clear_dead_leaves<T: MazeCoordType>(
    leaves: &mut Vec<usize>,
    visited: &[bool],
    nodes: &[MazeNode<T>],
) {
    leaves.retain(|&node_idx| {
        nodes[node_idx]
            .broken_neighbors
            .iter()
            .any(|&neighbor_idx| !visited[neighbor_idx])
    });
}

///! Chooses the next connection to be made in the maze.
/// This function evaluates potential connections from the current leaves based on various criteria
/// like twistiness, branchiness, and hallway likelihood, graded by `grade_potential_connection`.
/// It selects the highest-scoring connection as the next step in maze generation.
/// # Arguments
/// * `leaves` - A slice of current leaf indices in the maze.
/// * `visited` - A slice indicating whether each node in the maze has been visited.
/// * `maze` - A mutable reference to the maze structure.
/// * `weights` - Weights applied to different scoring metrics in the maze generation.
/// # Returns
/// Option<(usize, usize)> - The indices of the next connection, or None if no connection is found.
fn choose_next_connection<T: MazeCoordType>(
    leaves: &[usize],
    visited: &[bool],
    maze: &mut Maze<T>,
    weights: &MazeGenerationWeights,
) -> Option<(usize, usize)>
where
    f64: From<T>,
{
    leaves
        .iter()
        .flat_map(|&cur_leaf_idx| {
            maze.nodes[cur_leaf_idx]
                .broken_neighbors
                .clone()
                .into_iter()
                .filter_map(move |next_leaf_idx| {
                    if visited[next_leaf_idx] {
                        None
                    } else {
                        Some((cur_leaf_idx, next_leaf_idx))
                    }
                })
        })
        .max_by_key(|(cur_leaf_idx, next_leaf_idx)| {
            grade_potential_connection(maze, *cur_leaf_idx, *next_leaf_idx, weights)
        })
}

///! Generates a maze using the growing tree algorithm.
/// Starting from a random cell, the algorithm iteratively expands the maze by selecting
/// the next leaf based on a scoring system that considers twistiness, branchiness,
/// hallways, and randomness. The process continues until there are no more leaves to expand.
/// # Arguments
/// * `points` - Coordinates of nodes in the maze.
/// * `weights` - Weights dictating the style and complexity of the generated maze.
/// # Returns
/// `Maze<T>` - The generated maze.
/// Source: `<https://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm>`
pub fn growing_tree<T: MazeCoordType>(points: &[(T, T)], weights: &MazeGenerationWeights) -> Maze<T>
where
    f64: From<T>,
{
    let mut maze = Maze::<T>::new(points, false);
    let mut visited = vec![false; maze.nodes.len()];
    // Choose a random starter cell
    let starter_cell_idx = rand::thread_rng().gen_range(0..maze.nodes.len());
    visited[starter_cell_idx] = true;
    let mut leaves = vec![starter_cell_idx];
    #[cfg(feature = "verbose")]
    let bar = ProgressBar::new(points.len() as u64);

    while leaves.len() > 0 {
        let next_connection = choose_next_connection(&leaves, &visited, &mut maze, &weights);
        let (cur_leaf_idx, next_leaf_idx) = match next_connection {
            None => break,
            Some(connection) => connection,
        };

        maze.make_connection(cur_leaf_idx, next_leaf_idx);
        visited[next_leaf_idx] = true;
        leaves.push(next_leaf_idx);
        clear_dead_leaves(&mut leaves, &visited, &maze.nodes);
        #[cfg(feature = "verbose")]
        bar.inc(1);
    }
    maze
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::maze_generation::MazeNode;
    use crate::point_generation::make_point_grid;

    fn create_node(connected: &[usize], broken: &[usize]) -> MazeNode<u16> {
        MazeNode {
            pos: (0, 0),
            connected_neighbors: connected.iter().cloned().collect(),
            broken_neighbors: broken.iter().cloned().collect(),
        }
    }
    fn traverse<T: MazeCoordType>(maze: &Maze<T>, node_idx: usize, visited: &mut [bool]) {
        let node: &MazeNode<T> = &maze.nodes[node_idx];
        visited[node_idx] = true;
        let next_nodes: Vec<usize> = node
            .connected_neighbors
            .iter()
            .filter_map(|next_node| {
                if !visited[*next_node] {
                    Some(*next_node)
                } else {
                    None
                }
            })
            .collect();
        for next_node in next_nodes {
            traverse(maze, next_node, visited);
        }
    }
    ///! Check that a maze is connected (e.g. every point is reachable by every other point)
    fn is_connected<T: MazeCoordType>(maze: &Maze<T>) -> bool {
        let mut visited = vec![false; maze.nodes.len()];
        traverse(maze, 0, &mut visited);
        visited.iter().all(|x| *x)
    }

    #[test]
    fn test_growing_tree() {
        let points = make_point_grid(10, 10).unwrap();
        let weights = MazeGenerationWeights::default();
        let maze = growing_tree(&points, &weights);
        assert!(is_connected(&maze));
        let maze = growing_tree(&points, &weights);
        assert!(is_connected(&maze));
    }

    ///! Test growing tree with the minimum 3 points.
    #[test]
    fn test_growing_tree_min_points() {
        let points = vec![(0, 0), (1, 1), (0, 1)]; // Single point
        let weights = MazeGenerationWeights::default();
        let maze = growing_tree(&points, &weights);

        // Check that the maze contains only one node
        assert_eq!(maze.nodes.len(), 3);

        // Check that each node is connected
        assert!(maze.nodes.iter().all(|n| n.connected_neighbors.len() > 0));
        // Check that at least one connection was broken
        assert!(maze.nodes.iter().any(|n| n.connected_neighbors.len() < 2));
    }

    #[test]
    fn test_clear_dead_leaves_no_dead_leaves() {
        let nodes = vec![
            create_node(&[], &[1]), // Node 0
            create_node(&[], &[0]), // Node 1
        ];
        let mut leaves = vec![0];
        let visited = vec![true, false];

        clear_dead_leaves(&mut leaves, &visited, &nodes);

        // All leaves should still be present as none of them are dead ends
        assert_eq!(leaves, vec![0]);
    }

    #[test]
    fn test_clear_dead_leaves_all_dead_leaves() {
        let nodes = vec![
            create_node(&[1], &[]), // Node 0
            create_node(&[0], &[]), // Node 1
        ];
        let mut leaves = vec![0, 1];
        let visited = vec![true, true];

        clear_dead_leaves(&mut leaves, &visited, &nodes);

        // All leaves should be removed as they are all dead ends
        assert!(leaves.is_empty());
    }
}

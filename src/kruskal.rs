//! Kruskal's Algorithm for Maze Generation
//!
//! This module implements Kruskal's algorithm for maze generation with a unique adaptation.
//! Instead of randomly choosing the next connection, it selects the highest-scoring connection
//! based on various criteria like twistiness, branchiness, and hallway likelihood, influenced by weights.
//! The algorithm ensures the generation of a maze without loops, where every broken connection is a potential path.

use crate::{
    maze_generation::{grade_potential_connection, Maze, MazeCoordType, MazeGenerationWeights},
    MazeNode,
};
#[cfg(feature = "verbose")]
use indicatif::ProgressBar;

///! Retrieves all potential connections in a maze with initially broken neighbors.
/// This function considers every broken neighbor connection of each node as a potential connection
/// for the maze generation using Kruskal's algorithm.
/// # Arguments
/// * `nodes` - A slice of MazeNode representing the nodes in the maze.
/// # Returns
/// Vec<(usize, usize)> - A vector of tuples where each tuple represents a potential connection between nodes.
fn get_all_potential_connections<T: MazeCoordType>(nodes: &[MazeNode<T>]) -> Vec<(usize, usize)> {
    (0..nodes.len())
        .flat_map(|cur_leaf_idx| {
            nodes[cur_leaf_idx]
                .broken_neighbors
                .iter()
                .filter_map(move |&next_leaf_idx| {
                    if cur_leaf_idx < next_leaf_idx {
                        Some((cur_leaf_idx, next_leaf_idx))
                    } else {
                        None
                    }
                })
        })
        .collect()
}

///! Selects the next connection to make in the maze based on scoring criteria.
/// This function chooses the connection with the highest score according to `grade_potential_connection`,
/// considering twistiness, branchiness, hallway likelihood, and randomness.
/// # Arguments
/// * `connections` - A reference to a vector of potential connections.
/// * `maze` - A mutable reference to the Maze.
/// * `weights` - Weights influencing the scoring of potential connections.
/// # Returns
/// Option<(usize, usize)> - The next connection to make, or None if no suitable connection is found.
fn choose_next_connection<T: MazeCoordType>(
    connections: &Vec<(usize, usize)>,
    maze: &mut Maze<T>,
    weights: &MazeGenerationWeights,
) -> Option<(usize, usize)>
where
    f64: From<T>,
{
    connections
        .into_iter()
        .max_by_key(|(cur_leaf_idx, next_leaf_idx)| {
            grade_potential_connection(maze, *cur_leaf_idx, *next_leaf_idx, weights)
        })
        .copied()
}

///! Removes connections that would create loops in the maze.
/// This function iterates over existing connections and retains only those where the nodes
/// belong to different sets, ensuring that making these connections won't create loops in the maze.
/// # Arguments
/// * `connections` - A mutable reference to the vector of potential connections.
/// * `sets` - A slice representing the set to which each node currently belongs.
fn remove_dead_connections(connections: &mut Vec<(usize, usize)>, sets: &[usize]) {
    connections.retain(|&(a, b)| sets[a] != sets[b]);
}

///! Merges sets to reflect the connection of two nodes in the maze.
/// When a connection is made, the sets of the connected nodes are merged,
/// indicating that they are now part of the same connected component.
/// # Arguments
/// * `sets` - A mutable reference to the vector of node sets.
/// * `old_set_pos` - The position of the first node's set in the vector.
/// * `new_set_pos` - The position of the second node's set in the vector.
fn update_sets(sets: &mut Vec<usize>, old_set_pos: usize, new_set_pos: usize) {
    let set_to_replace = sets[old_set_pos];
    let replacement_set = sets[new_set_pos];
    sets.iter_mut()
        .filter(|set| **set == set_to_replace)
        .for_each(|set| *set = replacement_set);
}

///! Generates a maze using Kruskal's algorithm with a scoring-based connection selection.
/// This function generates a maze by iteratively connecting nodes. Unlike the traditional Kruskal's algorithm,
/// which selects connections randomly, this implementation chooses connections based on a scoring system,
/// taking into account various aspects of the maze's layout and applying specified weights.
/// Source: `<https://weblog.jamisbuck.org/2011/1/3/maze-generation-kruskal-s-algorithm>`
/// # Arguments
/// * `points` - Coordinates of nodes in the maze.
/// * `weights` - Weights for scoring potential connections.
/// # Returns
/// `Maze<T>` - The generated maze.
pub fn kruskal<T: MazeCoordType>(points: &[(T, T)], weights: &MazeGenerationWeights) -> Maze<T>
where
    f64: From<T>,
{
    let mut maze: Maze<T> = Maze::<T>::new(points, false);
    // Assign a unique set value to each node in the maze
    let mut sets: Vec<usize> = (0..maze.nodes.len()).collect();
    let mut potential_connections = get_all_potential_connections(&maze.nodes);
    let mut connections_remaining = potential_connections.len();
    #[cfg(feature = "verbose")]
    let bar = ProgressBar::new(connections_remaining as u64);
    while connections_remaining > 0 {
        let (cur_leaf_idx, next_leaf_idx) =
            choose_next_connection(&potential_connections, &mut maze, &weights).unwrap();

        maze.make_connection(cur_leaf_idx, next_leaf_idx);
        update_sets(&mut sets, cur_leaf_idx, next_leaf_idx);
        remove_dead_connections(&mut potential_connections, &sets);
        #[cfg(feature = "verbose")]
        bar.inc((connections_remaining - potential_connections.len()) as u64);
        connections_remaining = potential_connections.len();
    }
    #[cfg(feature = "verbose")]
    bar.finish();
    maze
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::maze_generation::MazeNode;
    use crate::point_generation::make_point_grid;

    fn traverse<T: MazeCoordType>(maze: &Maze<T>, node_idx: usize, visited: &mut [bool]) {
        let node: &MazeNode<T> = &maze.nodes[node_idx];
        visited[node_idx] = true;
        let next_nodes: Vec<usize> = node
            .connected_neighbors
            .iter()
            .filter_map(|next_node| {
                if !visited[*next_node] {
                    Some(*next_node)
                } else {
                    None
                }
            })
            .collect();
        for next_node in next_nodes {
            traverse(maze, next_node, visited);
        }
    }
    ///! Check that a maze is connected (e.g. every point is reachable by every other point)
    fn is_connected<T: MazeCoordType>(maze: &Maze<T>) -> bool {
        let mut visited = vec![false; maze.nodes.len()];
        traverse(maze, 0, &mut visited);
        visited.iter().all(|x| *x)
    }

    #[test]
    fn test_kruskal() {
        let points = make_point_grid(10, 10).unwrap();
        let weights = MazeGenerationWeights::default();
        let maze = kruskal(&points, &weights);
        assert!(is_connected(&maze));
    }

    #[test]
    fn test_kruskal_min_points() {
        let points = vec![(0, 0), (1, 1), (0, 1)];
        let weights = MazeGenerationWeights::default();
        let maze = kruskal(&points, &weights);

        assert_eq!(maze.nodes.len(), 3);
        assert!(maze.nodes.iter().all(|n| n.connected_neighbors.len() > 0));
        assert!(maze.nodes.iter().any(|n| n.broken_neighbors.len() > 0));
    }
}

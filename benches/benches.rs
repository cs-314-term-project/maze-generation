use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use maze_generator::{
    growing_tree,     // Import any required types
    kruskal,          // Replace with the actual module where your Kruskal function is defined
    make_point_cloud, // Import any required functions
    Maze,
    MazeGenerationWeights,
};
use std::time::Duration;

const MEASUREMENT_TIME_SECONDS: u64 = 1;
const SAMPLE_SIZE: usize = 50;

///! Compare processing time of kruskal and growing_tree algorithms.
fn bench_maze_algorithms(c: &mut Criterion) {
    let weights = MazeGenerationWeights::default();
    let mut group = c.benchmark_group("Maze Algorithms");
    group.measurement_time(Duration::from_secs(MEASUREMENT_TIME_SECONDS));
    group.sample_size(SAMPLE_SIZE);
    for n_points in [100, 300, 500, 700] {
        //Generate a point cloud with zero variance - just a spiral.
        let points = make_point_cloud(n_points, 1.0, 0.0);
        //Generate a fully-connected maze to calculate the number of connections in the point cloud
        let n_connections = Maze::new(&points, true).count_connections();
        ////pass n_connections to each bench, so that the generated graphs can use it as an x axis.
        group
            .bench_with_input(
                BenchmarkId::new("Kruskal", n_connections),
                &n_connections,
                |b, &_| {
                    b.iter(|| {
                        kruskal(&points, &weights);
                    });
                },
            )
            .bench_with_input(
                BenchmarkId::new("Growing Tree", n_connections),
                &n_connections,
                |b, &_| {
                    b.iter(|| {
                        growing_tree(&points, &weights);
                    });
                },
            );
    }
    group.finish();
}

criterion_group!(benches, bench_maze_algorithms);
criterion_main!(benches);

//! Maze Drawing Application
//!
//! This application is dedicated to rendering mazes as images.
//! It leverages the maze generation library to create complex mazes using either the growing tree or Kruskal's algorithms,
//! which can be customized with various weights like twistiness, branchiness, hallway preference, and randomness.
//! The mazes are then rendered into images with adjustable dimensions.
//!
//! Functionality:
//! - The `main` function parses command-line arguments to determine maze generation parameters and image dimensions.
//! - `draw_grid_maze` and `draw_pointcloud_maze` functions handle the drawing of mazes based on grid and point cloud
//!   configurations, respectively.
//! - `generate_maze` function generates the maze using the specified algorithm and maze generation weights.
//! - `make_filename` creates a filename for the output image based on the maze generation parameters.
//! - Utility functions like `maze_to_image_coordinates`, `draw_line`, and `draw_circle` assist in the rendering process.
//!
//! This application supports a range of customization options, allowing for the creation of unique and challenging mazes,
//! and outputs them as image files for visualization and further use.
//!
use image::{Rgb, RgbImage};
use line_drawing::Bresenham;
use maze_generator::{
    growing_tree, kruskal, make_point_cloud, make_point_grid, Maze, MazeCoordType,
    MazeGenerationWeights,
};
mod draw_maze_args;
use clap::{Parser, ValueEnum};
use draw_maze_args::{Algorithm, Args};

///! Generate a maze based on the given command line arguments.
fn main() {
    let args = Args::parse();
    let img = match args.dimensions {
        Some(_) => draw_grid_maze(&args),
        None => draw_pointcloud_maze(&args),
    };
    let filename = make_filename(&args);
    img.save(filename.clone()).unwrap();
    println!("Image saved to {filename}")
}

///! Draw a maze on a grid
fn draw_grid_maze(args: &Args) -> RgbImage {
    let points = match &args.dimensions {
        Some(dim) => make_point_grid(dim.0, dim.1).unwrap(),
        None => panic!("Grid maze requires \"dimensions\" argument to be defined."),
    };
    let maze = generate_maze(&points, args);
    draw_maze::<u16>(&maze, args.img_dimensions.clone().into())
}

///! Draw a maze on a point cloud
fn draw_pointcloud_maze(args: &Args) -> RgbImage {
    let points = match args.points {
        Some(n_points) => make_point_cloud(n_points, 0.3, 0.2),
        None => panic!("Point cloud maze requires \"points\" argument to be defined."),
    };
    let maze = generate_maze(&points, args);
    draw_maze::<f32>(&maze, args.img_dimensions.clone().into())
}

///! Generate a maze based on command line arguments
fn generate_maze<T: MazeCoordType>(points: &[(T, T)], args: &Args) -> Maze<T>
where
    f64: From<T>,
{
    let weights = MazeGenerationWeights {
        twists: args.wg_twist,
        branches: args.wg_branchy,
        hallways: args.wg_hallway,
        random: args.wg_random,
    };
    match args.algorithm {
        Algorithm::GrowingTree => growing_tree(&points, &weights),
        Algorithm::Kruskal => kruskal(&points, &weights),
    }
}

///! Create a filename from commandline arguments
fn make_filename(args: &Args) -> String {
    let dim_str = match &args.dimensions {
        Some(dim) => format!("d={x}x{y}", x = dim.0, y = dim.1),
        None => format!("p={points}", points = args.points.unwrap()),
    };
    format!(
        "examples/{name}_{dim_str}_a={alg}_w(t={wt},b={wb},h={wh},r={wr}).png",
        name = args.name,
        alg = args.algorithm.to_possible_value().unwrap().get_name(),
        wt = args.wg_twist,
        wb = args.wg_branchy,
        wh = args.wg_hallway,
        wr = args.wg_random
    )
}

///! Convert maze coordinates to image pixel coordinates.
fn maze_to_image_coordinates<T: MazeCoordType>(
    pos: (T, T),
    maze_dimensions: (T, T),
    image_bottom_left: (u32, u32),
    image_top_right: (u32, u32),
) -> (u32, u32) {
    assert!(image_bottom_left.0 < image_top_right.0);
    assert!(image_bottom_left.1 < image_top_right.1);
    let image_dimensions = (
        image_top_right.0 - image_bottom_left.0,
        image_top_right.1 - image_bottom_left.1,
    );
    let x_ratio: f64 = pos.0.into() / maze_dimensions.0.into();
    let y_ratio: f64 = pos.1.into() / maze_dimensions.1.into();
    let ratio = (x_ratio, y_ratio);

    (
        image_bottom_left.0 + (((image_dimensions.0 - 1) as f64) * ratio.0) as u32,
        image_bottom_left.1 + (((image_dimensions.1 - 1) as f64) * ratio.1) as u32,
    )
}

fn draw_line(img: &mut RgbImage, a: (u32, u32), b: (u32, u32), color: Rgb<u8>) {
    //Order the two points, to maintain consistent lines with a & b swapped.
    let (a, b) = if (a.0 > b.0) || (a.0 == b.0 && (a.1 > b.1)) {
        (b, a)
    } else {
        (a, b)
    };
    let a = (i64::from(a.0), i64::from(a.1));
    let b = (i64::from(b.0), i64::from(b.1));
    let line = Bresenham::new(a, b);
    for (x, y) in line {
        img.get_pixel_mut(x.try_into().unwrap(), y.try_into().unwrap())
            .0 = color.0;
    }
}

///! Draw a black and white maze on an image object with the given dimensions
fn draw_maze<T: MazeCoordType>(maze: &Maze<T>, image_dimensions: (u32, u32)) -> RgbImage {
    let mut img = RgbImage::new(image_dimensions.0, image_dimensions.1);
    img.fill(0);
    let image_bottom_left = (image_dimensions.0 / 20, image_dimensions.1 / 20);
    let image_top_right = (
        image_dimensions.0 - image_bottom_left.0,
        image_dimensions.1 - image_bottom_left.1,
    );
    for (a, node) in maze.nodes.iter().enumerate() {
        let node_a_pos =
            maze_to_image_coordinates(node.pos, maze.bounds, image_bottom_left, image_top_right);

        for &b in node.connected_neighbors.iter() {
            if b < a {
                continue;
            }
            let node_b_pos = maze_to_image_coordinates(
                maze.nodes[b].pos,
                maze.bounds,
                image_bottom_left,
                image_top_right,
            );
            draw_line(&mut img, node_a_pos, node_b_pos, Rgb { 0: [255, 255, 255] });
        }
    }
    img
}

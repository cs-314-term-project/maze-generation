use clap::{ArgGroup, Parser, ValueEnum};

use std::{
    fmt::{self, Display},
    str::FromStr,
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[clap(group(ArgGroup::new("dimension_or_points").args(&["dimensions", "points"]).required(true)))]
pub struct Args {
    /// Dimensions as X,Y
    #[clap(short, long, required_unless_present = "points")]
    pub dimensions: Option<Dimensions<u16>>,

    #[clap(short, long, value_parser, default_value_t =  Dimensions(512, 512))]
    pub img_dimensions: Dimensions<u32>,

    /// Number of points
    #[clap(short, long, required_unless_present = "dimensions")]
    pub points: Option<usize>,

    /// Algorithm to use
    #[clap(short, long, value_enum, default_value_t = Algorithm::GrowingTree)]
    pub algorithm: Algorithm,

    /// Twistiness preference
    #[clap(long, alias = "wt", default_value_t = 0.0, allow_hyphen_values = true)]
    pub wg_twist: f64,

    /// Branchyness preference
    #[clap(long, alias = "wb", default_value_t = 0.0, allow_hyphen_values = true)]
    pub wg_branchy: f64,

    /// Long hallway preference
    #[clap(long, alias = "wh", default_value_t = 0.0, allow_hyphen_values = true)]
    pub wg_hallway: f64,

    /// Randomness preference
    #[clap(long, alias = "wr", default_value_t = 1.0, allow_hyphen_values = false)]
    pub wg_random: f64,

    // Image filename prefix
    #[clap(short, long, default_value = "Maze")]
    pub name: String,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Dimensions<T>(pub T, pub T)
where
    T: Sized + FromStr + fmt::Display;

impl<T> fmt::Display for Dimensions<T>
where
    T: fmt::Display + FromStr,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{},{}", self.0, self.1)
    }
}

impl<T> FromStr for Dimensions<T>
where
    T: Sized + FromStr + fmt::Display,
{
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(',').collect();
        if parts.len() != 2 {
            return Err("Expected two values separated by a comma".to_string());
        }
        let x = parts[0]
            .parse::<T>()
            .map_err(|_| "Invalid value for width".to_string())?;
        let y = parts[1]
            .parse::<T>()
            .map_err(|_| "Invalid value for height".to_string())?;
        Ok(Dimensions(x, y))
    }
}

impl<T> Into<(T, T)> for Dimensions<T>
where
    T: Copy + FromStr + Display,
{
    fn into(self) -> (T, T) {
        (self.0, self.1)
    }
}

#[derive(Debug, ValueEnum, Clone)]
pub enum Algorithm {
    #[clap(name = "gt", alias = "growing tree")]
    GrowingTree,
    #[clap(name = "kr", alias = "kruskal")]
    Kruskal,
}

# Maze Generator by Danny Love

## Description
"Maze Generator" is a Rust library designed to generate mazes with a high degree of customization. The library features two main functions, `growing_tree` and `kruskal`, each accepting a vector of points and a collection of weights. The algorithms are templated to support both `f32` and `u16` types, facilitating the creation of both index-based and freeform maze structures.

A unique aspect of "Maze Generator" is its emphasis on customizable weights, including twistiness, branchiness, hallway preference, and randomness. These weights allow users to fine-tune the maze's characteristics, impacting its complexity and navigational challenges.

## Building and Running
To build the library:
```
cargo build --release --features verbose
```
The `verbose` feature is optional and includes a progress bar during maze generation.

To run the example:
```
cargo run --release --features verbose --example draw_maze -- <options>
```
Options include:
- `-d, --dimensions <DIMENSIONS>`: Specify dimensions as X,Y. If this argument is given, the maze will be generated on a grid. 
- `-i, --img-dimensions <IMG_DIMENSIONS>`: Image dimensions [default: 512,512].
- `-p, --points <POINTS>`: Number of points. If this argument is given, the maze will be generated on a point cloud.
- `-a, --algorithm <ALGORITHM>`: Algorithm to use [default: gt] [possible
 values: gt, kr].
    - gt (Growing Tree): The maze grows outward from a single point, choosing the point of growth at each step based on the given weights.
    - kr (Kruskal): The maze creates & combines sets of nodes until only one set remains. The next combination is chosen at each step based on the given weights.
- Weight preferences: 
    - Twistiness (`--wg-twist`): High numbers create squiggly mazes that turn frequently. Low will create long passageways.
    - Branchiness (`--wg-branchy`): High numbers will create mazes with a lot of branches in the path. Low will create a more labrynthian structure.
    - Hallways (`--wg-hallway`): High numbers will create mazes with long non-branching hallways, and fewer stubby dead ends. Low will make spiky mazes.
    - Randomness (`--wg-random`): High numbers will create mazes that disregard the other weights in favor of random selection. By default, this is the only non-zero weight.

- `-n, --name <NAME>`: Prefix to use in the saved file [default: Maze].

Note: `-p` and `-d` are mutually exclusive.

To test the library:
```
cargo test
```

To benchmark the maze algorithms:
```
cargo bench
```
After you run the benchmark, you can view a comparison of the benchmarks of the two algorithms at /target/criterion/Maze Algorithms/report/index.html.
## Examples
```
cargo run --release --example draw_maze -- -d 20,20 -a gt
```
![Example 1](images/Maze_d=20x20_a=gt_w_t=0,b=0,h=0,r=1_.png "Example 1")
```
cargo run --example draw_maze -- -d 20,20 -a kr
```
![Example 2](images/Maze_d=20x20_a=kr_w(t=0,b=0,h=0,r=1).png "Example 2")
```
cargo run --release --features verbose --example draw_maze -- -p 10000 -i 2048,2048 --wg-hallway 1 --wg-
random 0 --wg-branchy 1
```
![Example 3](images/Maze_p=10000_a=gt_w(t=0,b=1,h=1,r=0).png "Example 3")



## Testing
Testing in this library is moderately extensive, with a focus on core functionality. Tests were conducted to check the behavior of each maze algorithm with varying inputs, including minimal point scenarios and different algorithm-specific properties like the handling of dead ends in growing trees and the distribution of points in generated mazes. Additionally, tests were written to check the proper distribution of point clouds, using statistical analysis. These rigorous tests ensured that the mazes generated met the expected criteria, providing a high level of confidence in the library's functionality.

## Reflections
Getting the mazes to work efficiently, especially Kruskal's algorithm, was challenging. Optimizations included addressing unnecessary collections, using efficient data types, and profiling with tools like `perf`. Handling ownerships and lifetimes was also an ordeal, due to all of the interconnected node.s The use of indices instead of direct node references simplified ownership and lifetimes substantially at the cost of some readability.

## License

Copyright 2023 Danny Love

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
